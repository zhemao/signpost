from __future__ import division
from settings import FLYER_LIMIT
import math
from models import Board
from urlparse import urlparse
import urllib
from django.template.loader import render_to_string
from pygments import highlight
from pygments.lexers import guess_lexer
from pygments.formatters import HtmlFormatter

# Earth's mean radius in meters
EARTH_RADIUS = 6371000

def havsin(theta):
	'''The haversine function. See http://en.wikipedia.org/wiki/Haversine.'''
	return math.sin(theta/2)**2

def calc_dist(src, dest):
	'''Calculate the distance between two points on earth represented
	   as (lat,lon) tuples using the haversine formula.
	   See http://en.wikipedia.org/wiki/Great-circle_distance'''
	dlat = src[0] - dest[0]
	dlon = src[1] - dest[1]
	
	dtheta = 2 * math.asin(math.sqrt(havsin(dlat) 
				+ math.cos(src[0])*math.cos(dest[0])*havsin(dlon)))
	
	return EARTH_RADIUS * dtheta

def find_nearest_board(lat, lon):
	offset = 0.5
	minlat = lat - offset
	minlon = lon - offset
	maxlat = lat + offset
	maxlon = lon + offset
	
	boards = Board.objects.filter(lat__gt=minlat, lat__lt=maxlat, 
			lon__gt=minlon, lon__lt=maxlon)
	
	# there can't be any distance greater than the earth's circumference
	mindist = 2 * EARTH_RADIUS * math.pi
	minind = -1		
	for i, board in enumerate(boards):
		dist = calc_dist((lat, lon), (board.lat, board.lon))
		if dist < mindist:
			mindist = dist
			minind = i
	
	if minind == -1:
		return None
	return boards[minind]
	
def wrap_audio(url):
	return "<audio src=\"%s\" controls=\"controls\"></audio>" % (url,)
	
def wrap_video(url):
	return "<video src=\"%s\" controls=\"controls\"></video>" % (url,)
	
def wrap_link(url):
	return "<a href=\"%s\">%s</a>" % (url, url)
	
def wrap_code(url):
	f = urllib.urlopen(url)
	code = f.read()
	f.close()
	lexer = guess_lexer(code)
	html = highlight(code, lexer, HtmlFormatter())
	return html
	
def wrap_image(url):
	return "<img src=\"%s\" alt=\"image\"/>" % (url,)
	
def flyer_to_html(flyer):
	func_table = {
		'A': wrap_audio,
		'V': wrap_video,
		'L': wrap_link,
		'I': wrap_image,
		'C': wrap_code
	}
	func = func_table.get(flyer.type)
	link = func(flyer.url) if func else ''
	
	return render_to_string('flyer.html', {'link': link, 'id':flyer.id,
			'caption': flyer.caption})
	
def board_to_dict(board):
	d = {'id': board.id, 'name': board.name, 
		'lat': board.lat, 'lon': board.lon}
	d['flyers'] = []
	flyers = board.flyer_set.order_by('created').reverse()[:FLYER_LIMIT]
	
	for fly in flyers:
		flyd = {'id': fly.id, 'url': fly.url, 'type': fly.type, 
				'caption': fly.caption, 'html': flyer_to_html(fly)}
		d['flyers'].append(flyd)
	
	return d
