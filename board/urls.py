from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
	url(r'^/?$', 'signpost.board.views.index'),
	url(r'^flyers$', 'signpost.board.views.flyers')
)
