from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response
from signpost.board.helpers import *
import json
from django.template import RequestContext

# Create your views here.

def flyers(request):
	if 'lat' not in request.GET or 'lon' not in request.GET:
		raise Http404
	lat = float(request.GET['lat'])
	lon = float(request.GET['lon'])
	board = find_nearest_board(lat, lon)
	
	if not board:
		raise Http404
	
	d = board_to_dict(board)
	
	return HttpResponse(json.dumps(d), mimetype="application/json")
			
def index(request):
	return render_to_response('index.html', context_instance=RequestContext(request))
	
	
