from django.db import models

# Create your models here.

class Board(models.Model):
	name = models.CharField(max_length=128)
	lat = models.FloatField()
	lon = models.FloatField()
	
	def __unicode__(self):
		return self.name
	
class Flyer(models.Model):
	FLYER_TYPES = (
		('L', 'Link'),
		('I', 'Image'),
		('A', 'Audio'),
		('V', 'Video'),
		('C', 'Code'),
	)

	url = models.URLField()
	type = models.CharField(max_length=1, choices=FLYER_TYPES)
	caption = models.TextField(default='')
	board = models.ForeignKey(Board)
	created = models.DateTimeField(auto_now_add=True)
	
	def __unicode__(self):
		trunc_len = 25
		if len(self.caption) < trunc_len:
			return self.caption
		return self.caption[:trunc_len]+'...'
		
