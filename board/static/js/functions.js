
function flyer_callback(data, textStatus, jqXHR){
	$("#header").html("<h1>Latest flyers for "+data['name']+"</h1>");
	
	for(var i = data.flyers.length-1; i>-1; i--){
		flyer = data.flyers[i];
		previous = $("#"+flyer.id);
		if(previous.length > 0){
			var fly_html = $(flyer.html).html();
			if(previous.html() !== fly_html){
				previous.html(fly_html);
			}
		}
		else{
			$("#main").prepend($(flyer.html));
		}
	}
}

function request_flyers(position){
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;
	var baseurl = "/flyers";
	var url = baseurl + '?' + 'lat='+lat + '&lon='+lon;
	$.get(url, null, flyer_callback);
}

function get_location(){
	navigator.geolocation.getCurrentPosition(request_flyers);
	setTimeout("get_location();", 5000);
}
