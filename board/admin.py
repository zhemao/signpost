from signpost.board.models import Board, Flyer
from django.contrib import admin

class BoardAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['name']}),
		('Coordinates', {'fields': ['lat','lon']})
	]
	
admin.site.register(Board, BoardAdmin)
admin.site.register(Flyer)
